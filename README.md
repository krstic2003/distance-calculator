# Distance Calculator

## System requirements - Laravel 8

- PHP >= 7.3 (**PHP 8.x recommended**)
- BCMath PHP Extension
- Ctype PHP Extension
- Fileinfo PHP Extension
- JSON PHP Extension
- Mbstring PHP Extension
- OpenSSL PHP Extension
- PDO PHP Extension
- Tokenizer PHP Extension
- XML PHP Extension
- GIT
- Composer
- MySQL
- Apache or Nginx
