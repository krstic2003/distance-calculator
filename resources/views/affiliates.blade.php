@include('head')
    <body class="antialiased">
        <div class="relative flex items-top justify-center min-h-screen bg-gray-100 dark:bg-gray-900 sm:items-center py-4 sm:pt-0">

            <div class="max-w-6xl mx-auto sm:px-6 lg:px-8">


                <div class="mt-8 bg-white dark:bg-gray-800 overflow-hidden shadow sm:rounded-lg">
                    <div class="grid sm:px-6" style="min-width: 800px; color:white;">
                        <h1>Affiliates < 100 km from  Dublin office(53.3340285, -6.2535495)</h1>
                        <table style="width:100%; text-align:left;">
                            <tr>
                                <th>ID</th>
                                <th>Name</th>
                                <th>Latitude</th>
                                <th>Longitude</th>
                                <th>Distance (km)</th>
                            </tr>

                        @foreach ($data as $d)
                            <tr>
                                <td>{{ $d['affiliate_id'] }}</td>
                                <td>{{ $d['name'] }}</td>
                                <td>{{ $d['latitude'] }}</td>
                                <td>{{ $d['longitude'] }}</td>
                                <td>{{ number_format($d['distance'], 2, '.', ',') }}</td>
                            </tr>
                        @endforeach
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </body>
@include('footer')
