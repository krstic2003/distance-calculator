<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Storage;

class Affiliate extends Model
{
    use HasFactory;

    /**
     * Get data from json and filter it to be < 100km from office
     *
     * @return array
     */
    public static function getNearData(): array
    {
        $path = storage_path() . "/affiliates.txt";
        $json_contents = file($path,FILE_IGNORE_NEW_LINES);

        $json_final = [];
        foreach($json_contents as $jc){
            $jc = json_decode($jc, true);

            // Office coordinates - 53.3340285, -6.2535495 can be moved to .env or DB
            $distance = self::greatCircleDistance(53.3340285, -6.2535495, (float)$jc['latitude'], (float)$jc['longitude']);
            if($distance <= 100){
                $jc['distance'] = $distance;
                array_push($json_final, $jc);
            }

        }
        return $json_final;
    }

    /**
     * Calculates the great-circle distance between two points
     *
     * @param float $latitudeFrom Latitude of start point
     * @param float $longitudeFrom Longitude of start point
     * @param float $latitudeTo Latitude of target point
     * @param float $longitudeTo Longitude of target point
     * @return float Distance between points in [km]
     */
    protected static function greatCircleDistance(
        float $latitudeFrom, float $longitudeFrom, float $latitudeTo, float $longitudeTo): float
    {
        // in km
        $earthRadius = 6371;

        // convert from degrees to radians
        $latFrom = deg2rad($latitudeFrom);
        $lonFrom = deg2rad($longitudeFrom);
        $latTo = deg2rad($latitudeTo);
        $lonTo = deg2rad($longitudeTo);

        $lonDelta = $lonTo - $lonFrom;
        $a = pow(cos($latTo) * sin($lonDelta), 2) +
        pow(cos($latFrom) * sin($latTo) - sin($latFrom) * cos($latTo) * cos($lonDelta), 2);
        $b = sin($latFrom) * sin($latTo) + cos($latFrom) * cos($latTo) * cos($lonDelta);

        $angle = atan2(sqrt($a), $b);
        return $angle * $earthRadius;
    }
}
