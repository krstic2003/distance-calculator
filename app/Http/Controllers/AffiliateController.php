<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Affiliate;
use Illuminate\View\View;

class AffiliateController extends Controller
{
    /**
     * Get data from Affiliate model
     *
     * @return Illuminate\View\View
     */
    public function show(): View
    {
        $data = Affiliate::getNearData();
        return view('affiliates')->with([
            'data' => $data,
        ]);
    }
}
