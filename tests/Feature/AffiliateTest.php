<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Tests\TestCase;

class AffiliateTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testAffiliates(): void
    {
        $response = $this->get('/near-affiliates');
        $response->assertStatus(200);

        $path = storage_path() . "/affiliates.txt";
        $json_contents = file($path, FILE_IGNORE_NEW_LINES);
        $this->assertNotEmpty($json_contents);
        $this->assertIsArray($json_contents);

        $expected = [
            [
            "latitude" => "52.986375",
            "affiliate_id" => 12,
            "name" => "Yosef Giles",
            "longitude" => "-6.043701",
            "distance" => 41.111086711707,
            ],
            [
            "latitude" => "54.0894797",
            "affiliate_id" => 8,
            "name" => "Addison Lister",
            "longitude" => "-6.18671",
            "distance" => 84.11741984004,
            ],
            [
            "latitude" => "53.038056",
            "affiliate_id" => 26,
            "name" => "Moesha Bateman",
            "longitude" => "-7.653889",
            "distance" => 98.936752121557,
            ],
            [
            "latitude" => "53.1229599",
            "affiliate_id" => 6,
            "name" => "Jez Greene",
            "longitude" => "-6.2705202",
            "distance" => 23.496927301724,
            ],
            [
            "latitude" => "53.2451022",
            "affiliate_id" => 4,
            "name" => "Inez Blair",
            "longitude" => "-6.238335",
            "distance" => 9.9397330376503,
            ],
            [
            "latitude" => "53.1302756",
            "affiliate_id" => 5,
            "name" => "Sharna Marriott",
            "longitude" => "-6.2397222",
            "distance" => 22.674973202301,
            ],
            [
            "latitude" => "53.008769",
            "affiliate_id" => 11,
            "name" => "Isla-Rose Hubbard",
            "longitude" => "-6.1056711",
            "distance" => 37.486210279603,
            ],
            [
            "latitude" => "53.1489345",
            "affiliate_id" => 31,
            "name" => "Maisha Mccarty",
            "longitude" => "-6.8422408",
            "distance" => 44.25120987554,
            ],
            [
            "latitude" => "53",
            "affiliate_id" => 13,
            "name" => "Terence Wall",
            "longitude" => "-7",
            "distance" => 62.091479368892,
            ],
            [
            "latitude" => "52.966",
            "affiliate_id" => 15,
            "name" => "Veronica Haines",
            "longitude" => "-6.463",
            "distance" => 43.240819251523,
            ],
            [
            "latitude" => "54.180238",
            "affiliate_id" => 17,
            "name" => "Gino Partridge",
            "longitude" => "-5.920898",
            "distance" => 96.601682017804,
            ],
            [
            "latitude" => "53.0033946",
            "affiliate_id" => 39,
            "name" => "Kirandeep Browning",
            "longitude" => "-6.3877505",
            "distance" => 37.837426004713,
            ],
            [
            "latitude" => "54.133333",
            "affiliate_id" => 24,
            "name" => "Ellena Olson",
            "longitude" => "-6.433333",
            "distance" => 89.661771310804,
            ],
            [
            "latitude" => "53.74452",
            "affiliate_id" => 29,
            "name" => "Alvin Stamp",
            "longitude" => "-7.11167",
            "distance" => 72.792237229438,
            ],
            [
            "latitude" => "53.761389",
            "affiliate_id" => 30,
            "name" => "Kingsley Vang",
            "longitude" => "-7.2875",
            "distance" => 83.211721821348,
            ],
            [
            "latitude" => "54.080556",
            "affiliate_id" => 23,
            "name" => "Ciara Bannister",
            "longitude" => "-6.361944",
            "distance" => 83.31605254609,
            ]
        ];
        $data = $response->getOriginalContent()->getData();
        $this->assertEquals(sort($expected), sort($data['data']));
        foreach($data['data'] as $d){
            $this->assertLessThan(101,$d['distance']);
        }
    }
}
